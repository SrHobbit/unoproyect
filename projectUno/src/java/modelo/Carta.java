/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author LENOVO
 */
public class Carta {
    private int numero;
    private String color;
    private int ID;

    public Carta(int numero, String color, int ID) {
        this.numero = numero;
        this.color = color;
        this.ID = ID;
    }
    
     public Carta() {
        this.numero = 0;
        this.color = "";
        this.ID = 0;
    }
    
    

    public int getId() {
        return ID;
    }

    public void setId(int ID) {
        this.ID = ID;
    }



    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
}
