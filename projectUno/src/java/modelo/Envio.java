/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;


/**
 *
 * @author LENOVO
 */
public class Envio {
    private Jugador jugador;
    private Carta cartaEnJuego;
    private int idJugadorEnTurno;
    private ArrayList<String[]> informacionJugadores;
    private int idGanador = 0;

    public int getIdGanador() {
        return idGanador;
    }

    public void setIdGanador(int idGanador) {
        this.idGanador = idGanador;
    }

    public Envio(Jugador jugador, Carta cartaEnJuego, int idJugadorEnTurno, ArrayList<String[]> numeroCartasJugadores) {
        this.jugador = jugador;
        this.cartaEnJuego = cartaEnJuego;
        this.idJugadorEnTurno = idJugadorEnTurno;
        this.informacionJugadores = numeroCartasJugadores;
    }
    
    public Envio() {
        this.jugador = null;
        this.cartaEnJuego = null;
        this.idJugadorEnTurno = 0;
        this.informacionJugadores = null;
    }

    public ArrayList<String[]> getNumeroCartasJugadores() {
        return informacionJugadores;
    }

    public void setNumeroCartasJugadores(ArrayList<String[]> numeroCartasJugadores) {
        this.informacionJugadores = numeroCartasJugadores;
    }
   

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }


    public Carta getCartaEnJuego() {
        return cartaEnJuego;
    }

    public void setCartaEnJuego(Carta cartaEnJuego) {
        this.cartaEnJuego = cartaEnJuego;
    }

    public int getIdJugadorEnTurno() {
        return idJugadorEnTurno;
    }

    public void setIdJugadorEnTurno(int idJugadorEnTurno) {
        this.idJugadorEnTurno = idJugadorEnTurno;
    }
    
    public ArrayList<String[]> getInformacionJugadores() {
        return informacionJugadores;
    }

    public void setInformacionJugadores(ArrayList<String[]> informacionJugadores) {
        this.informacionJugadores = informacionJugadores;
    }
}
