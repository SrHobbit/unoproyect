/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LENOVO
 */
public class Jugador {
    private int id;
    private String nombre;
    private Carta cartaElegida;
    public List<Carta> baraja;

    public Jugador(int id, String nombre) {
        this.id=id;
        this.nombre = nombre;
        this.cartaElegida= new Carta();
        this.baraja= new ArrayList();
    }
    
    public Jugador() {
        this.id=0;
        this.nombre = "";
        this.cartaElegida= null;
        this.baraja= null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Carta> getBaraja() {
        return baraja;
    }

    public void setBaraja(ArrayList<Carta> baraja) {
        this.baraja = baraja;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Carta getCartaElegida() {
        return cartaElegida;
    }

    public void setCartaElegida(Carta cartaElegida) {
        this.cartaElegida = cartaElegida;
    }
    
    
}
