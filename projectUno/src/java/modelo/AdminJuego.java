/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import controller.Utils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author LENOVO
 */
public class AdminJuego {
    public static ArrayList<Carta> maso =new ArrayList<>();
    public static Carta cartaEnJuego= new Carta();
    public static ArrayList<Jugador> jugadores =new ArrayList<>();
    public static ArrayList<Carta> cartasUsadas =new ArrayList<>();
    public static Jugador jugadorEnTurno= new Jugador();
    public static Jugador jugadorRecibido;
    public static Jugador jugadorRobo;

    
    //Constructor que crea el maso, lo revuelve y pone una carta en mesa
    /*public AdminJuego() {
        crearMaso();
        //Método que revuelve el maso
        Collections.shuffle(maso);
        //cartaEnJuego=maso.get(0);
        maso.remove(0);
        barajar();
    }*/
    
    public static void crearMaso(){
        
        int id=0;
        for(int i=0; i<10; i++){
            for(int j=0; j<8; j++){
                
                String color="";
                
                switch(j){
                    case 0:
                        color="r";
                    break;
                    case 1:
                        color="az";
                    break;
                    case 2:
                        color="a";
                    break;
                    case 3:
                        color="v";
                    break;
                    case 4:
                        color="r";
                    break;
                    case 5:
                        color="az";
                    break;
                    case 6:
                        color="a";
                    break;
                    case 7:
                        color="v";
                    break;
                }
                Carta card=new Carta(i,color,id);
                maso.add(card);
                id++;

            }
        }
    }
    
    //Método que revuelve el maso
    public static void revolverMaso(){
        Collections.shuffle(maso);
        cartaEnJuego=maso.get(0);
        maso.remove(0);
    }
    
    //Método que le da a cada jugador una baraja
    public static void barajar(){
        for(Jugador jug : jugadores){
            for(int j=0; j<7;j++){
                jug.baraja.add(maso.get(0));
                maso.remove(0);
            }
        }
    }
    
    //Método que va cambiando de turnos
    public static void siguienteTurno(){
      if(jugadorEnTurno.getId()<jugadores.size()){
          int i = jugadorEnTurno.getId()+1;
          jugadorEnTurno.setId(i);
      }
      else{
          jugadorEnTurno.setId(1);
      }
    }
    //Método que verifica que la carta elegida por el jugador coincida con la carta 
    
    public static boolean verificarCarta(Carta cartaJugador){
        
        return cartaJugador.getColor().equals(cartaEnJuego.getColor()) || cartaJugador.getNumero()== cartaEnJuego.getNumero();
        
    }
    
    //Metodo que le da al jugador una carta del mazo 
    
    //Metodo que Añade usuarios nuevos 
    
       public static Jugador convertirUsuario(String x) {
           
           
           jugadorRecibido = (Jugador)Utils.fromJson(x, Jugador.class);
           
           return jugadorRecibido;

    }
     
       public static Jugador convertirUsuarioRobar(String x) {
           
           
           jugadorRobo = (Jugador)Utils.fromJson(x, Jugador.class);
           
           return jugadorRobo;

    }
  
       
    
       //Método que reinicia las variables
       public static void reiniciar(){
           maso =new ArrayList<>();
           new Carta();
           jugadores =new ArrayList<>();
           jugadorEnTurno.setId(0);
       }
       
       
     
           public static ArrayList<String[]> numeroCartasJugadores(){
           
           ArrayList<String[]> cartasJugadores= new ArrayList<>();
           int i=0;
                jugadores.forEach((jugador)-> {
                    String datos[]={""+jugador.baraja.size(),""+jugador.getId(),jugador.getNombre()};
                    cartasJugadores.add(datos);
                    
                    });
 
           
           return cartasJugadores;
          
       }      
           
           public static void removerCarta(){
            
        Iterator<Carta> it = jugadorRecibido.baraja.iterator();
        Carta carta;
                while(it.hasNext()) {
                    carta = it.next();
                    if(carta.getId()==jugadorRecibido.getCartaElegida().getId()){
                        it.remove();
                        break;
                    }
                }
        }
           
        public static void robarCarta(){
            
            if(!maso.isEmpty()){
                
                jugadorRobo.baraja.add(maso.get(0));
                maso.remove(0);
            }else{
                
                maso=AdminJuego.cartasUsadas;
                AdminJuego.revolverMaso();
                
                jugadorRobo.baraja.add(maso.get(0));
                maso.remove(0);
            }
            
        }
        
        public static Jugador elegirGanador(Jugador jugador){
            
            
            if(jugadorRecibido.getId()==jugador.getId()){
                
                return jugador;
            }
            
            return null;
        }
}
