
package controller;
//codigo servidor
import java.io.IOException;
import javax.enterprise.context.ApplicationScoped;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.AdminJuego;
import modelo.Envio;
import modelo.Jugador;

@ApplicationScoped
@ServerEndpoint(value = "/pajarito/")

public class MyWS{

    public  static final ArrayList<MyWS> conexiones =new ArrayList<>();
    private Session sesionWS;
    private static final AtomicInteger idConexion= new AtomicInteger(0);
    private Jugador jugador;


    public MyWS() {
        idConexion.getAndIncrement();
    }
    
    
    @OnOpen
    public void onOpen(Session session) {
        
   
        
        if(!verificarConexiones() && RecepcionDatos.contador<=5){
        System.out.println("Creando Jugador y conexion");
        crearJugador();
        this.sesionWS=session;
        conexiones.add(this); 
        }
        
        if(verificarConexiones()){
        iniciarJuego();
        //Se le envia a cada jugador su baraja, maso y la carta en juego
            publicarGlobalmente(0);
            System.out.println(AdminJuego.jugadores.get(0).getNombre());
            System.out.println(AdminJuego.jugadores.get(1).getNombre());
            System.out.println(AdminJuego.jugadores.get(2).getNombre());
            System.out.println(AdminJuego.jugadores.get(3).getNombre());
            System.out.println("Hay "+AdminJuego.jugadores.size()+AdminJuego.jugadorRecibido.getNombre());
        }
       

    }

    @OnError
    public void onError(Throwable t) {
    }

    @OnClose
    public void onClose() {
        conexiones.remove(this);
        AdminJuego.jugadores.remove(jugador);
        //if(idConexion.intValue()==0)
        System.out.println(jugador.getNombre()+" ha abandonado la partida.");
        //Si se vuelve a iniciar el juego las variables se reestablecen
        if(conexiones.isEmpty()){
            AdminJuego.reiniciar();
            idConexion.set(0);
        }
    }

    @OnMessage
    public static void onMessage(String texto) {
        
        if(AdminJuego.jugadorRecibido.getId()== AdminJuego.jugadorEnTurno.getId() || AdminJuego.jugadorRobo.getId()== AdminJuego.jugadorEnTurno.getId()){
            
            System.out.println("Los ID son iguales");
            
            if(AdminJuego.verificarCarta(AdminJuego.jugadorRecibido.getCartaElegida()) && AdminJuego.jugadorRobo == null){
                
                System.out.println("Las cartas coinciden");
                int idGanador = 0;
                
                AdminJuego.cartaEnJuego=AdminJuego.jugadorRecibido.getCartaElegida();
                AdminJuego.cartasUsadas.add(AdminJuego.cartaEnJuego);
                AdminJuego.removerCarta();
                
                if(AdminJuego.jugadorRecibido.baraja.size()==1){
                   
                    idGanador=AdminJuego.jugadorRecibido.getId();
                }
                AdminJuego.jugadores.set(AdminJuego.jugadorEnTurno.getId()-1,AdminJuego.jugadorRecibido);
                AdminJuego.siguienteTurno();
                publicarGlobalmente(idGanador);
                
                
            }
            else{
                System.out.println("Robando carta");
                AdminJuego.robarCarta();
                AdminJuego.jugadores.set(AdminJuego.jugadorEnTurno.getId()-1,AdminJuego.jugadorRobo);
                publicarGlobalmente(0);
                AdminJuego.jugadorRobo = null;
                
            }
            
        }

    }
    
           public void iniciarJuego(){
           
            //Creación y revuelve el maso
            AdminJuego.crearMaso();
            AdminJuego.revolverMaso();
            //En el caso de que hayan 4 jugadores se dan las barajas
            AdminJuego.barajar();
            //Se le asigna el turno al jugador 1
            AdminJuego.jugadorEnTurno.setId((int) (Math.random()*5+1)) ;
            
        
       }
           
       public static boolean verificarConexiones(){
           
        return conexiones.size()==5;
        
       }
       
       public void crearJugador(){
           
        jugador=new Jugador(idConexion.intValue(),AdminJuego.jugadorRecibido.getNombre());
        AdminJuego.jugadores.add(jugador);
       }
       
    
           
           


     public static void publicarGlobalmente(int idGanador) {
        for(int i=0; i<5; i++){
            MyWS conexion=conexiones.get(i);
            if(conexion.sesionWS.isOpen()){
                try {
                     Envio envio=new Envio(AdminJuego.jugadores.get(i),AdminJuego.cartaEnJuego,AdminJuego.jugadorEnTurno.getId(),AdminJuego.numeroCartasJugadores());
                     
                     if(idGanador!=0){
                         
                         envio.setIdGanador(idGanador);
                     }
                    
                    conexion.sesionWS.getBasicRemote().sendText(Utils.toJson(envio));
                } catch (IOException ex) {
                    Logger.getLogger(MyWS.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
     
    
}
