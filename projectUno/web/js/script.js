var websocket;

var _WEBSOCKETS_SERVER_URL = "ws://" + document.location.hostname + ":8080/projectUno/";

var jugador;
var cartaEnJuego;
var maso;
var idJugadorEnTurno;
var informacionJugadores;
var n = '';
var idGanador;


$('.ui.modal').modal('show');

function connectWS() {
    // notar el protocolo.. es 'ws' y no 'http'
    var wsUri = _WEBSOCKETS_SERVER_URL + "pajarito/";
    websocket = new WebSocket(wsUri); // creamos el socket

    websocket.onopen = function(evt) { // manejamos los eventos...
        console.log("Conectado..."); // ... y aparecerá en la pantalla
    };

    websocket.onmessage = function(evt) { // cuando se recibe un mensaje
        let msg = evt.data;
        msg = JSON.parse(msg);
        console.log(msg);
        jugador = msg.jugador;
        cartaEnJuego = msg.cartaEnJuego;
        idJugadorEnTurno = msg.idJugadorEnTurno;
        informacionJugadores = msg.informacionJugadores;
        idGanador=msg.idGanador;
        //implementar toda la lógica del mensaje! OJO QUE ACÁ SE ASUME QUE LLEGA EN JSON
        inyectar();
    };

    websocket.onerror = function(evt) {
        //$("#mensajes").append("<br>Ha ocurrido un error y no se ha podido conectar el WS :,v<br>");
        console.log("oho!.. error:" + evt.data);
    };

    websocket.onclose = function(evt) {
        console.log(evt);
    };
}

function enviarMensaje(text) {
    console.log("Enviando:" + text);
    websocket.send(text);
}


//connectWS();

function inyectar() {
    $('#salaDeEspera').css("visibility", "hidden");
    $('#jugadorNombre').empty();
    $('#imagenEnJuego').empty();
    $('#cartasJugador').empty();
    $('#nombreContrincante').empty();
    $('#accionJugador').empty();
    $('#jugadorNombre').append(jugador.nombre);
    $('#izquierda').empty();

    informacionJugadores.forEach(element => jugadorEnTurno(element));

    $('#imagenEnJuego').append("<img id='imgEnJuego' numero='" + cartaEnJuego.numero + "' color='" + cartaEnJuego.color + "' src='img/" + cartaEnJuego.numero + cartaEnJuego.color + ".png'>");
    var m = "";
    for (var i = 0; i < jugador.baraja.length; i++) {
        m += "<img numero='" + jugador.baraja[i].numero + "' color='" + jugador.baraja[i].color + "'id='" + jugador.baraja[i].ID + "'src='img/" + jugador.baraja[i].numero + jugador.baraja[i].color + ".png'>";
    }
    $('#cartasJugador').append(m);

    informacionJugadores.forEach(jugador => $('#nombreContrincante').append(jugador[2]));

    informacionJugadores.forEach(element => ingresarContrincante(element));

    $('#izquierda').append(n);
    n='';
    
 
        
  informacionJugadores.forEach(element => ganador(element));
        
 
    
}

function ganador(element){
    
    
    if (jugador.id == idJugadorEnTurno &&  element[0]== 0) {
        
        $("#modalGanador").css("visibility","visible");
         $('#nombreGanador').append("Felicidades, Eres el Ganador");
        
    } else if (jugador.id != idJugadorEnTurno && element[0] == 0) {
    
    $("#modalGanador").css("visibility","visible");
    $('#nombreGanador').append(element[2]+" Es el Ganador");
   
    }
}

function jugadorEnTurno(element) {

    if (jugador.id == idJugadorEnTurno && idJugadorEnTurno == element[1]) {
        $('#accionJugador').append('Es tu turno');
    } else if (jugador.id != idJugadorEnTurno && idJugadorEnTurno == element[1]) {
        var posicion = idJugadorEnTurno - 1;
        console.log(informacionJugadores[posicion]);
        var x = informacionJugadores[posicion];
        $('#accionJugador').append('Es turno de ' + x[2]);
    }

}



function ingresarContrincante(element) {
    if (element[1] != jugador.id) {
        n += '<div class="top attached ui segment" id="colorCentro">\
                    <div>\
                        <H4 id="nombreContrincante">' + element[2] + '</H4>\
                        <div class="detail">\
                            <div id = "numeroCartasContrincante" class="ui mini images">\\n\
                            ' + cartasContrincantes(element) + '\
                            </div>\
                        </div>\
                    </div>\
                </div>\
                <br>';
    }
}

function cartasContrincantes(element) {

    var m = '';

    for (var i = 0; i < element[0]; i++) {
        m += '<img class="ui image" src="img/posterior.jpg">';
    }

    return m;
}
;

$(document).ready(function(){
    
    $("#cartasJugador").on("click","img", function(e) {
        
    e.preventDefault();
    e.stopImmediatePropagation();
    
    var pulsadaNumero = $(this).attr("numero");
    var pulsadaColor = $(this).attr("color");
    var pulsadaID = $(this).attr("id");
    
   
    if (idJugadorEnTurno == jugador.id) {
        
  
        
        
        if ($("#imgEnJuego").attr("numero") == pulsadaNumero || $("#imgEnJuego").attr("color") == pulsadaColor) {
            jugador.cartaElegida.numero = parseInt(pulsadaNumero, 10);
            jugador.cartaElegida.color = pulsadaColor;
            jugador.cartaElegida.ID = pulsadaID;
            extraerInfo(jugador);
            msg = null;
            cartaEnJuego = null;
            informacionJugadores = null;
        } else {
            $("#turno").empty();
            alert("selecciones otra carta padre");
        }
        
    } else {
        $("#turno").empty();
        $("#turno").append("RECUERDE : Turno de Jugador#" + idJugadorEnTurno);
    }
    //Comparar la imagen seleccionada con la de la mesa
    //si son iguales entonces mandarle al WS el jugador con la carta seleccionada
    //si son diferentes decirle que seleccione otra
    //si no es su turno recordarle que tiene que esperar
});
    
});

$('#robar').click(function(){
    if (idJugadorEnTurno == jugador.id){
         robar();
    }
    else{
        alert("No es su turno");
    }
   
});

function robar(){
    jugador.cartaElegida.numero = 0;
    jugador.cartaElegida.color = "";
    var dataToSend = JSON.stringify(jugador);
    $.ajax({
        url: "RobarCarta",
        type: "POST",
        data: dataToSend,
        contentType: "application/json; charset=utf-8",
        success: function(result){
 
            console.log("Petición recibida");
            msg = null;
            cartaEnJuego = null;
        },
        error: function(result){
            
            console.log("Ocurrio algun error");
        }
    });
}